# Personal Website Eric Rios



## My Personal Website Repository - IDS 721 Cloud Computing

This is my personal Website!!! This specific repository is the duke-AI-ML variant. It's Mini Project 1 from week 1.

Link : https://my-personal-website-eric-rios-public-dukeaiml-id-814b4bf2657f01.gitlab.io/


![Alt text](image-1.png)

## Description
This project uses a tool called Zola to build a static website. My used theme is albatros.

## Installation
Here is the [zola website](https://www.getzola.org/).

Use the documentation there to build zola and serve a website, per their terminology. Edit the html pages to your liking, a little bit at a time. In a short while, you'll find that you built something cool. 

## Authors and acknowledgment
Thanks [Hugo](https://git.42l.fr/HugoTrentesaux/Albatros) for making the albatros template.

## Project status
Updated last February 2024.
