+++
title = "Wiki"
weight = 2
sort_by = "weight"

[taxonomies]
authors = ["Albatros",]
tags = ["wiki"]

[extra.translations]
fr = "wiki/"
es = "wiki/"
+++

# Wiki

Welcome page of the wiki.

As you see there is langage flags to switch to brother websites. They can be given in `[extra.translations]` block.

```toml
[extra.translations]
fr = "wiki/"
es = "wiki/
```

There is an automatic breadcrumb allowing to navigate wiki pages.