+++
title = "Shortcodes"
date = 2022-12-04
description = "Some available shortcodes"

[extra]
thumbnail = "/img/albatros.svg"

[taxonomies]
authors = ["Albatros",]
tags = ["shortcodes"]
category = ["website"]

+++

<br>
<br>

{% note(type="warning") %}
This is a warning note
{% end %}


{% note(type="info") %}
This is a info note
{% end %}


{% note(type="ok") %}
This is a ok note
{% end %}


{% note(type="defautl") %}
This is a default note
{% end %}

