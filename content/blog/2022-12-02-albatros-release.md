+++
title = "Albatros release"
date = 2022-12-02
description = "After a lot of time working on Duniter website, I'm finally releasing its Zola theme in the name of Albatros."

[extra]
thumbnail = "/avatars/HugoTrentesaux.png"

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["zola", "website"]
category = ["website"]

+++
I am releasing Albatros Theme