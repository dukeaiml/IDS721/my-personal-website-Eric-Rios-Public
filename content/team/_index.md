+++
title = "Collaborators"
date = 2020-07-29
weight = 5
template = "custom/team.html"
page_template = "authors/page.html"

[extra]
# controls the list of team (file name)
authors = [
    "HugoTrentesaux",
    "TEMPLATE",
    "Albatros",
    ]
+++

Birds of a feather...